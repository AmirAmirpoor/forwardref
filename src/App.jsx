import { useRef } from "react";
import Input from "./Input";

import "./App.css";

function App() {
  const inputRef = useRef(null);

  const focusInput = () => {
    inputRef.current.focus();
  };

  return (
    <div className="App">
      <h1>Hello There</h1>

      <button onClick={focusInput}>focus the input</button>
      <br />

      <Input ref={inputRef} type="text" />
    </div>
  );
}

export default App;
